defmodule IrcWebClient.IRC do
  require Logger
  require GenServer
  alias IrcWebClient.IRC.Message

  @moduledoc """
  Manages a lightweight connection to one IRC server
  """

  defmodule Config do
    use IrcWebClient.Schema

    schema "configs" do
      field :host, :string
      field :port, :integer
      field :name, :string
      field :pass, :string
      field :nick, :string
      field :ssl?, :boolean

      belongs_to :user, IrcWebClient.User

      timestamps()
    end
    #defstruct name: "webclient_test_bot",
    #          pass: nil,
    #          host: "localhost",
    #          port: 6667,
    #          nick: "webclient_test_bot",
    #          ssl?: true
  end
  defmodule IRCState do
    @moduledoc false
    defstruct config: %Config{},
              socket: nil,
              state: :disconnected,
              server_caps: %MapSet{},
              enabled_caps: %MapSet{},
              parent: nil
  end

  # Client API
  @spec start_link(%Config{}) :: GenServer.on_start()
  def start_link(conf), do: GenServer.start_link(__MODULE__, %IRCState{config: conf, parent: self()})


  # Server API
  @desired_caps MapSet.new(["message-tags"])

  @spec init(%IRCState{}) :: {:ok, %IRCState{}}
  def init(state) do
    IO.puts("connecting to server: #{state.config.host}:#{state.config.port}")

    res =
      if state.config.ssl? do
        :ssl.connect(to_charlist(state.config.host), state.config.port, packet: :line)
      else
        :gen_tcp.connect(to_charlist(state.config.host), state.config.port, packet: :line)
      end

    case res do
      {:ok, socket} ->

        state = %{state | socket: socket}

        # Next we start registration
        # 1. CAP LS 302
        :ok = send_msg(state, %Message{command: "CAP", params: ["LS", "302"]})

        # 2. PASS if applicable
        if state.config.pass != nil do
          :ok = send_msg(state, %Message{command: "PASS", params: [state.config.pass]})
        end

        # 3. NICK
        # TODO verify nick initialization is are successful
        :ok = send_msg(state, %Message{command: "NICK", params: [state.config.nick]})

        # 4. username
        :ok =
          send_msg(
            state,
            %Message{
              command: "USER",
              params: [state.config.name, "0", "*", state.config.nick]
            }
          )

        # TODO include timeout
        # We would like a timeout to detect server disconnections
        {:ok, %{state | state: :logging_on}}

      {:error, reason} ->
        Logger.warn("Cannot connect to server, #{reason}")
        Process.exit(self(), :normal)
    end
  end

  defp send_msg(%IRCState{socket: socket, config: %Config{ssl?: false}}, msg) do
    Message.serialize(msg)
    |> IO.inspect()
    |> to_charlist
    |> (&:gen_tcp.send(socket, &1)).()
  end

  defp send_msg(%IRCState{socket: socket}, msg) do
    Message.serialize(msg)
    |> IO.inspect()
    |> to_charlist
    |> (&:ssl.send(socket, &1)).()
  end

  defp handle_msg(
         %Message{command: "CAP", params: params},
         %IRCState{state: :logging_on} = state
       ) do
    case params do
      [_nick, "LS", caps] ->
        case String.split(caps, " ", trim: true) do
          # If the server has a multi line list of caps to send us then the
          # first element of the list is a `*`. In this case we should add it to
          # our cap list and move on
          ["*" | caps] ->
            %{state | server_caps: MapSet.union(MapSet.new(caps), state.server_caps)}

          # We have our cap list, and now we should request which ones we want
          # from the server.
          caps ->
            state = %{state | server_caps: MapSet.union(MapSet.new(caps), state.server_caps)}

            caps = MapSet.intersection(state.server_caps, @desired_caps)

            unless MapSet.size(caps) == 0 do
              :ok =
                send_msg(state, %Message{
                  command: "CAP",
                  params: ["REQ" | MapSet.to_list(caps)]
                })
            else
              if state.state == :logging_on do
                send_msg(state, %Message{command: "CAP", params: ["END"]})
              end
            end

            state
        end

      # Get the caps that were enabled and go ahead with negotiation
      [_nick, "ACK", caps] ->
        state = %{state | enabled_caps: MapSet.union(MapSet.new(caps), state.enabled_caps)}

        if state.state == :logging_on do
          send_msg(state, %Message{command: "CAP", params: ["END"]})
        end

        state

      # Go ahead with negotiation
      [_nick, "NAK", _caps] ->
        send_msg(state, %Message{command: "CAP", params: ["END"]})
        state

      # TODO edit our list
      [_nick, "NEW", caps] ->
        %{state | server_caps: MapSet.union(MapSet.new(caps), state.server_caps)}

      [_nick, "DEL", caps] ->
        %{state | server_caps: MapSet.difference(state.server_caps, MapSet.new(caps))}

      unknown ->
        Logger.warn("Unknown parameter to `CAP` command: #{inspect(unknown)}")
        state
    end
  end

  # 001: rpl_welcome
  defp handle_msg(
         %Message{command: "001", params: [nick | _]} = msg,
         state
       ) do
    # Forward our message down
    send_parent(state, {:irc, state.config.name, msg})
    # but grab the nick out!
    %{state | config: %{state.config | nick: nick}}
  end

  # err_nomotd, we consider this the same as rpl_endofmotd
  defp handle_msg(
         %Message{command: "442"} = msg,
         %IRCState{state: :logging_on} = state
       ) do
    handle_msg(%{msg | command: "376"}, state)
  end

  defp handle_msg(%Message{command: "PING"} = msg, state) do
    send_msg(state, %{msg | command: "PONG"})
    state
  end

  # rpl_endofmotd, this lets us know we are connected and have received the
  # greeting and are now free to perform actions
  defp handle_msg(
         %Message{command: "376"} = msg,
         %IRCState{state: :logging_on} = state
       ) do
    Logger.info("Logged into server")
    state = %{state | state: :connected}
    send_parent(state, {:irc_logged_in})
    send_parent(state, {:irc, state.config.name, msg})
    state
  end

  # catch all function
  defp handle_msg(msg, state) do
    send_parent(state, {:irc, msg})
    state
  end

  # send a record to the parent process
  defp send_parent(state, record), do: send(state.parent, {__MODULE__, self(), record})

  def handle_cast({:send, msg}, state) do
    send_msg(state, msg)
    {:noreply, state}
  end

  def handle_info({:ssl_closed, sock}, state) do
    handle_info({:tcp_closed, sock}, state)
  end

  def handle_info({:tcp_closed, _sock}, state) do
    {:stop, "TCP connection to server closed", state}
  end

  def handle_info({:ssl_error, sock}, state) do
    handle_info({:tcp_closed, sock}, state)
  end

  def handle_info({:tcp_error, _sock, reason}, state) do
    {:stop, "TCP error: #{inspect(reason)}", state}
  end

  def handle_info({:ssl, sock, line}, state) do
    handle_info({:tcp, sock, line}, state)
  end

  def handle_info({:tcp, _sock, line}, state) do
    msg =
      line
      |> to_string
      |> Message.parse()

    unless msg do
      {:stop, "Cannot parse line `#{to_string(line)}` from server #{inspect(state.conf.name)}",
       state}
    else
      {:noreply,
       msg
       # |> IO.inspect()
       |> handle_msg(state)}
    end
  end
end
