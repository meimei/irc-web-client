defmodule IrcWebClient.Repo do
  use Ecto.Repo,
    otp_app: :irc_web_client,
    adapter: Ecto.Adapters.Postgres
end
