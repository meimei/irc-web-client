defmodule IrcWebClient.IRC.User do
  @moduledoc """
  An IRC user
  """
  defstruct [:name, user: "", host: ""]
end
