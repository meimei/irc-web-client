defmodule IrcWebClient.IRC.Message do
  @moduledoc """
  Methods for parsing IRC messages
  """
  alias IrcWebClient.IRC.User

  @typedoc """
  The type of a IRC message
  """
  @type t :: %__MODULE__{
          tags: %{String.t() => String.t()},
          user: %User{} | nil,
          command: String.t(),
          params: [String.t()]
        }

  @enforce_keys [:command]
  defstruct [:command, :user, tags: %{}, params: []]

  defp unescape(string) do
    Regex.replace(~r/\\([^\\:srn]|$)/, string, "\\1")
    # First, give each escaped backslash another representation
    |> String.replace("\\\\", "\\e")
    # Replace the other strings
    |> String.replace("\\:", ";")
    |> String.replace("\\s", " ")
    |> String.replace("\\r", "\r")
    |> String.replace("\\n", "\n")
    # Finally escape the backslash
    |> String.replace("\\e", "\\")
  end

  @spec parse(String.t()) :: %__MODULE__{} | nil
  def parse(message) do
    message_regex = ~r/^
      # Tags are optional and signified by the leading `@`
      (@(?<tags>[^\ \r\n]*)\ +)?
      # There may also be a prefix
      (:(?<prefix>[^\ \r\n]*)\ +)?
      # The command is either a sequence of characters or a 3 digit code
      (?<command>([[:alpha:]]+|[[:digit:]]{3}))\ *
      # The paramaters are the arguments to the command and comprise the rest
      # of the message
      (?<params>[^\r\n]*)?/x

    captures = Regex.named_captures(message_regex, message)

    if captures == nil do
      nil
    else
      tags =
        String.split(Map.get(captures, "tags"), ";", trim: true)
        |> Enum.map(&String.split(&1, "=", parts: 2, trim: true))
        # We also have to escape values
        |> Enum.reduce(%{}, fn
          [key, val], acc -> Map.put(acc, key, unescape(val))
          [key], acc -> Map.put(acc, key, nil)
        end)

      params =
        case String.split(Map.get(captures, "params"), ":", parts: 2) do
          ["", last] -> [last]
          # XXX here we use split/3 because IRC messages should only be
          # deliminated by spaces and not other unicode whitespace
          [middle, last] -> String.split(middle, " ", trim: true) ++ [last]
          [middle] -> String.split(middle, " ", trim: true)
          _ -> []
        end

      user =
        case Map.get(captures, "prefix") do
          "" ->
            nil

          prefix ->
            case String.split(prefix, "!") do
              [server] ->
                %User{name: server}

              [nick, info] ->
                [name, host] = String.split(info, "@")
                %User{name: nick, user: name, host: host}
            end
        end

      %__MODULE__{tags: tags, user: user, command: Map.get(captures, "command"), params: params}
    end
  end

  defp escape(string) do
    string
    |> String.replace("\\", "\\\\")
    |> String.replace(";", "\\:")
    |> String.replace(" ", "\\s")
    |> String.replace("\r", "\\r")
    |> String.replace("\n", "\\n")
  end

  @doc "Serialize a Message to a string"
  @spec serialize(__MODULE__.t()) :: String.t()
  def serialize(message) do
    if message.tags == %{} do
      ""
    else
      for {key, val} <- message.tags, into: "@" do
        if val == nil do
          key <> ";"
        else
          key <> "=" <> escape(val) <> ";"
        end
      end
      # remove final semicolon 
      |> String.slice(0..-2)
      # add separator
      |> Kernel.<>(" ")
    end <>
      case message.user do
        nil ->
          ""

        %User{name: server, user: "", host: ""} ->
          ":" <> server <> " "

        %User{name: nick, user: "", host: host} ->
          ":" <> nick <> "@" <> host <> " "

        %User{name: nick, user: user, host: host} ->
          ":" <> nick <> "!" <> user <> "@" <> host <> " "
      end <>
      message.command <>
      case message.params do
        [] ->
          ""

        params ->
          {last, middle} = List.pop_at(params, -1)
          " " <> Enum.join(middle, " ") <> " :" <> last
      end <> "\r\n"
  end
end
