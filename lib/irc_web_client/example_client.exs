import IO
alias IrcWebClient.IRC

defmodule IrcWebClient.ExampleClient do
  alias IrcWebClient.IRC.User
  alias IrcWebClient.IRC.Message

  def print_loop() do
    receive do
      {IrcWebClient.IRC, _pid, {:irc,
       %Message{
         user: %User{name: name},
         command: "PRIVMSG",
         params: [_dest, msg]
       }}} ->
        puts("#{name}:#{msg}")
        print_loop()
    end
  end

  def write_loop(pid, dest) do
    msg =
      gets("")
      |> String.trim_trailing("\n")

    GenServer.cast(
      pid,
      {:send,
       %Message{
         command: "PRIVMSG",
         params: [dest, msg]
       }}
    )

    write_loop(pid, dest)
  end
end

alias IrcWebClient.ExampleClient

IO.puts("--- Example Client ---")

server =
  IO.gets("server:")
  |> String.trim_trailing("\n")

port =
  IO.gets("port:  ")
  |> Integer.parse()
  |> elem(0)

{:ok, pid} =
  IRC.start_link(%IRC.Config{
    host: server,
    port: port,
  })

receive do
  {IrcWebClient.IRC, _pid, {:irc_logged_in}} -> nil
after
  10000 -> IEx.Helpers.flush
           raise "Could not log on"
end

dest =
  IO.gets("Logged in\nWhere would you like to send messages to?:")
  |> String.trim_trailing("\n")

# Next send all messages to the console to IRC
spawn(fn -> ExampleClient.write_loop(pid, dest) end)

ExampleClient.print_loop()
