defmodule IrcWebClient.User do
  use IrcWebClient.Schema
  require Logger
  require GenServer
  alias IrcWebClient.IRC.Config
  use Pow.Ecto.Schema,
    user_id_field: :name

  @moduledoc """
  This module represents a single user of the service.
  The user is connected to any number of servers and will receive messages from
  them.

  XXX should we be doing {password,email}-validation and other account services
  elsewhere?
  """

  @type t :: %__MODULE__{
    id: Ecto.UUID.t(),
    name: String.t(),
    configs: [Config.t()] | %Ecto.Association.NotLoaded{},
    inserted_at: DateTime.t(),
    updated_at: DateTime.t()
  }

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "users" do
    field :name, :string
    #field :email, :string
    has_many :configs, Config
    pow_user_fields()

    timestamps()
  end

  #def changeset(%__MODULE__{} = user, attrs) do
  #  user
  #  |> pow_changeset(attrs)
  #end

  #defp unique_email(changeset) do
  #  changeset
  #  |> validate_format(
  #    :email,
  #    ~r/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9-\.]+\.[a-zA-Z]{2,}$/
  #  )
  #  |> validate_length(:email, max: 255)
  #  |> unique_constraint(:email)
  #end

end
