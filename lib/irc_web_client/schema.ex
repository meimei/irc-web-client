defmodule IrcWebClient.Schema do
  @moduledoc """
  The Schema used for all entries in our database.
  This uses v4 UUIDs instead of auto-incrementing IDs to
  identify records.
  """
  defmacro __using__(_) do
    quote do
      use Ecto.Schema
      import Ecto.Changeset
      @primary_key {:id, :binary_id, autogenerate: true}
      @foreign_key_type :binary_id
    end
  end
end
