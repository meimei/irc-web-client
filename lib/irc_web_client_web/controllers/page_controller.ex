defmodule IrcWebClientWeb.HomePageController do
  use IrcWebClientWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
