defmodule IrcWebClientWeb.SessionView do
  use IrcWebClientWeb, :view

  def render("info.json", %{info: token}) do
    %{access_token: token}
  end
end
