defmodule TestMacros do
  use ExUnit.Case
  alias IrcWebClient.IRC.Message, as: Message

  defmacro parse_test(msg, do: {:==, _ctx, [a, b]}) do
    quote do
      test unquote(msg) do
        TestMacros.gen_test(unquote(a), unquote(b))
      end
    end
  end

  def gen_test(str, %Message{} = msg) when is_binary(str) do
    gen_test(msg, str)
  end

  def gen_test(%Message{} = msg, str) when is_binary(str) do
    assert Message.parse(str) == msg
    assert msg |> Message.serialize() |> Message.parse() == msg
  end
end

defmodule IRCMessageParserTest do
  require TestMacros
  use ExUnit.Case
  import TestMacros
  alias IrcWebClient.IRC.Message, as: Message
  alias IrcWebClient.IRC.User, as: User
  doctest Message

  parse_test("Simple message", do: %Message{command: "foo"} == "foo")

  parse_test "Simple message with arguments" do
    %Message{
      tags: %{},
      command: "foo",
      params: ["bar", "baz", "asdf"]
    } == "foo bar baz asdf"
  end

  parse_test "Correctly handles prefix" do
    ":pounce foo bar baz" ==
      %Message{
        tags: %{},
        user: %User{name: "pounce"},
        command: "foo",
        params: ["bar", "baz"]
      }
  end

  parse_test "Correctly handles traling param" do
    ":pounce foo bar baz :asdf aoeu" ==
      %Message{
        tags: %{},
        user: %User{name: "pounce"},
        command: "foo",
        params: ["bar", "baz", "asdf aoeu"]
      }
  end

  parse_test "Correctly handles traling param with colon" do
    ":pounce foo bar baz ::aoeu" ==
      %Message{
        tags: %{},
        user: %User{name: "pounce"},
        command: "foo",
        params: ["bar", "baz", ":aoeu"]
      }
  end

  parse_test "Handles tags" do
    "@a=b;c=32;k;rt=ql7 foo" ==
      %Message{
        tags: %{"a" => "b", "c" => "32", "k" => nil, "rt" => "ql7"},
        command: "foo",
        params: []
      }
  end

  parse_test "Unescapes tags" do
    ~S"@a=b\\\\and\nk;c=72\s45;d=gh\:764 foo" ==
      %Message{
        tags: %{"a" => "b\\\\and\nk", "c" => "72 45", "d" => "gh;764"},
        command: "foo",
        params: []
      }
  end

  parse_test "Can handle both tags and prefices" do
    "@c;h=;a=b :quux ab cd" ==
      %Message{
        tags: %{"c" => nil, "h" => nil, "a" => "b"},
        user: %User{name: "quux"},
        command: "ab",
        params: ["cd"]
      }
  end

  parse_test "Tab is not considered whitespace" do
    ":cool\tguy foo bar baz" ==
      %Message{
        tags: %{},
        user: %User{name: "cool\tguy"},
        command: "foo",
        params: ["bar", "baz"]
      }
  end

  parse_test "Tab is not considered whitespace in tags" do
    "@a=ab\tcd;b=2;c foo bar baz" ==
      %Message{
        tags: %{"a" => "ab\tcd", "b" => "2", "c" => nil},
        command: "foo",
        params: ["bar", "baz"]
      }
  end

  parse_test "Drop non valid escape characters" do
    "@foo=\\a\\b\\c\\d\\ COMMAND" ==
      %Message{
        tags: %{"foo" => "abcd"},
        command: "COMMAND",
        params: []
      }
  end

  parse_test "More escaping" do
    "@foo=\\\\\\\\\\:\\\\s\\s\\r\\n COMMAND" ==
      %Message{
        tags: %{"foo" => "\\\\;\\s \r\n"},
        command: "COMMAND",
        params: []
      }
  end

  parse_test "Odd escape characters in prefix" do
    ":coolguy!~ag@n\x02et\x0305w\x0fork.admin PRIVMSG foo :bar baz" ==
      %Message{
        tags: %{},
        user: %User{name: "coolguy", user: "~ag", host: "n\x02et\x0305w\x0fork.admin"},
        command: "PRIVMSG",
        params: ["foo", "bar baz"]
      }
  end
end
