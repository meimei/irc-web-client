# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project. If another project depends on
# this project, this file won't be loaded nor affect the parent
# project. For this reason, if you want to provide default
# values for your application for 3rd-party users, it should be
# done in your "mix.exs" file.
use Mix.Config


# You can configure your application as:
#
#     config :irc_web_client, key: :value
#
# and access this configuration in your application as:
#
#     Application.get_env(:irc_web_client, :key)
#
# You can also configure a 3rd-party app:
#
#     config :logger, level: :info
#

# It is also possible to import configuration files, relative to this
# directory. For example, you can emulate configuration per environment
# by uncommenting the line below and defining dev.exs, test.exs and such.
# Configuration from the imported file will override the ones defined
# here (which is why it is important to import them last).
#
#     import_config "#{Mix.env()}.exs"
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration

config :irc_web_client,
  ecto_repos: [IrcWebClient.Repo],
  generators: [binary_id: true]

# Configures the endpoint
config :irc_web_client, IrcWebClientWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "FVItUQtf7baipfqS4IxkHoW54G6oXlnuxcOhHwRaN6kXRmkLSZJGOh/uz//hQngM",
  render_errors: [view: IrcWebClientWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: IrcWebClient.PubSub,
  live_view: [signing_salt: "rLOachek"]

config :irc_web_client, IrcWebClient.Repo,
  migration_primary_key: [name: :id, type: :binary_id]

config :irc_web_client, :pow,
  user: IrcWebClient.User,
  repo: IrcWebClient.Repo,
  web_module: IrcWebClientWeb

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
