defmodule IrcWebClient.Repo.Migrations.CreateConfigs do
  use Ecto.Migration

  def change do
    create table(:configs) do
      add :user_id, references(:users, on_delete: :delete_all)
      add :host, :string
      add :port, :integer
      add :name, :string
      add :pass, :string
      add :nick, :string
      add :ssl?, :boolean

      timestamps()
    end
  end
end
