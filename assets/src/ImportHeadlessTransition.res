@react.component @bs.module("@headlessui/react")

external make: (
  ~show: bool=?,
  ~enter: string=?,
  ~enterFrom: string=?,
  ~enterTo: string=?,
  ~leave: string=?,
  ~leaveFrom: string=?,
  ~leaveTo: string=?,
  ~children: React.element =?,
  ~\"as": React.component<{..}>=?,
) => React.element = "Transition"
