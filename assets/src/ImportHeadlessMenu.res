type menuProp = { "open" : bool }

@react.component @bs.module("@headlessui/react")
external make: (
  ~className: string=?,
  ~fontSize: string=?,
  ~children: menuProp => React.element =?,
  ~\"as": string=?,
) => React.element = "Menu"


module Button = {
  @react.component @bs.module("@headlessui/react") @scope("Menu")
  external make: (
    ~children: React.element=?,
    ~className: string=?,
    ~\"as": string=?,
  ) => React.element = "Button"
}
module Items = {
  @react.component @bs.module("@headlessui/react") @scope("Menu")
  external make: (
    ~static: bool=?,
    ~className: string=?,
    ~children: React.element =?,
    ~\"as": string=?,
  ) => React.element = "Items"
}
module Item = {
  @react.component @bs.module("@headlessui/react") @scope("Menu")
  external make: (
    ~disabled: bool=?,
    ~children: {..} => React.element =?,
    ~\"as": string=?,
  ) => React.element = "Item"
}
