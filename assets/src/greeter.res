@react.component
let make = () => {
  <section className="phx-hero">
    <h1> {React.string("Welcome to Phoenix with Rescript and React!")} </h1>
  </section>
}
