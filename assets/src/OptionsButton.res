/* This example requires Tailwind CSS v2.0+ */
//import { Fragment } from 'react'
//import { Menu, Transition } from '@headlessui/react'
//import { ChevronDownIcon } from '@heroicons/react/solid'

module Menu = ImportHeadlessMenu
module Transition = ImportHeadlessTransition
module Icons = ImportIcons

let style = (a) => (a["active"] ? "bg-gray-100 text-gray-900" : "text-gray-700") ++ " block px-4 py-2 text-sm"

@react.component
let make = () => {
  <Menu \"as"="div" className="relative inline-block text-left mr-4">
    ...((p) => 
      <>
        <div>
          <Menu.Button className="inline-flex justify-center w-full rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-indigo-500">
            {"Options"->React.string}
            <Icons.ChevronDownIcon className="-mr-1 ml-2 h-5 w-5" \"aria-hidden"="true" />
          </Menu.Button>
        </div>

        <Transition
            show={p["open"]}
            \"as"={React.Fragment.make}
            enter="transition ease-out duration-100"
            enterFrom="transform opacity-0 scale-95"
            enterTo="transform opacity-100 scale-100"
            leave="transition ease-in duration-75"
            leaveFrom="transform opacity-100 scale-100"
            leaveTo="transform opacity-0 scale-95"
            >
          <Menu.Items static=true
              className="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none divide-y divide-gray-100"
              >

            <div className="py-1">
              {Belt.Array.map([1,2,3], id =>
                <Menu.Item key={Belt.Int.toString(id)}>
                  {(a) =>
                    <a href="#"
                        className={style(a)}>
                      {j`Option $id`->React.string}
                    </a>
                  }
                </Menu.Item>
              )->React.array}
          </div>
          <div className="py-1">
            <Menu.Item key="edit">
            {(a)=>
              <a href="/edit_account"
                  className={style(a)}>
                {"Modify Account"->React.string}
              </a>
            }
            </Menu.Item>
            <Menu.Item key="logout">
            {(a) =>
              <form action="/logout" method="post" className={style(a)}>
                <button type_="submit" >
                  {"Sign Out"->React.string}
                </button>
              </form>
            }
            </Menu.Item>
          </div>
        </Menu.Items>
      </Transition>
    </>
  )
  </Menu>
}
