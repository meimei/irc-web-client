type asType<'props> = Lit(string) | Comp(React.component<'props>)
