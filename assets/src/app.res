%%raw("import '../css/app.css'")

module App = {
  @react.component
  let make = () => {
    <>
      <Header/>
      <Greeter />
    </>
  }
}

switch ReactDOM.querySelector("#app") {
| Some(_el) => switch ReactDOM.querySelector("#main") {
  | Some(root) => ReactDOM.render(<App />, root)
  | None => ()
}
| None => ()
}
