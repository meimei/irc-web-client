module ChevronDownIcon = {
  @react.component @bs.module("@heroicons/react/solid")
  external make: (
    ~children: React.element =?,
    ~className: string=?,
    ~\"aria-hidden": string=?,
  ) => React.element = "ChevronDownIcon"
}
