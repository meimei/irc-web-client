module.exports = {
  purge: [
    '../**/*.eex',
    './src/**/*.res',
    './src/**/*.bs.js',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
